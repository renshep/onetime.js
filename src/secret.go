package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/renshep/envopt/go/envopt"
	"gitlab.com/renshep/kvstore/go/kvstore"
	"os"
	"strings"
	"sync"
)

/*
GET    /           = shows web ui
GET    /picnic.min.css = css for ui

POST   /msg/       = creates new message and returns ID of message

GET    /msg/:msgid = shows UI with button to retrieve message
DELETE /msg/:msgid = retruns the original message and deletes it from the server
*/

// -= CONFIG =-
// port to listen on
var port = ":8081"

// url path to get/set messages
const msgpath = "/msg"

// directory of public html/css
var srvDir = "./public"

// -= Storage Config =-
// type of store - default to filesystem
var kvstore_type = "filesystem"

// file path to store messages
var kvstore_path = "./secrets"

// combined encryption and ivkey size (in bits)
const keyBitSize = 256

// cipher to use
// const cipherName = "aes-128-cfb"
// hash to use
// const hashName = "sha256"
// submit html file to use
var submitHtmlFile = "./public/submit.html"

// view html file to use
var viewHtmlFile = "./public/view.html"

// css file to use
var cssFileDark = "./public/pico.classless.min.css"

// -= CODE =-
// constants
const smsgpath = msgpath + "/"
const smsgpathid = smsgpath + ":key/:iv"
const keyByteSize = keyBitSize / 8
const keyByteSizeHalf = keyByteSize / 2

// globals
var secrets map[string][]byte
var sm sync.Mutex
var kvs kvstore.KVStore
var disableMemoryCache string

// functions
func getRandomKey() (key []byte, iv []byte, err interface{}) {
	newKey := make([]byte, keyByteSizeHalf)
	newIV := make([]byte, keyByteSizeHalf)
	if _, err := rand.Read(newKey); err != nil {
		return nil, nil, err
	}
	if _, err := rand.Read(newIV); err != nil {
		return nil, nil, err
	}
	return newKey, newIV, nil
}
func getKeyHash(key []byte, iv []byte) string {
	myHash := sha256.New()
	myHash.Write(key)
	return hex.EncodeToString(myHash.Sum(iv))
}
func loadMessage(skey, siv string) []byte {
	// get key and iv from hkey
	key, _ := hex.DecodeString(skey)
	iv, _ := hex.DecodeString(siv)
	// get hash from key
	keyhash := getKeyHash(key, iv)
	// load from memory
	sm.Lock()
	defer sm.Unlock()
	secret, havesecret := secrets[keyhash]
	// load from disk if needed
	if havesecret != true {
		fmt.Println("secret not in memory, loading secret from kvstore")
		sdata, loaderr := kvs.Load(keyhash, "error")
		if loaderr != nil {
			fmt.Println("kvs.Load error:", loaderr)
		}
		secret = []byte(sdata)
	}
	// decrypt message
	block, err := aes.NewCipher(key)
	if err != nil {
		// TODO handle error
		return nil
	}
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(secret, secret)
	// delete message from memory
	delete(secrets, keyhash)
	// delete message from disk
	delErr := kvs.Delete(keyhash)
	if delErr != nil {
		fmt.Println("kvs.Delete error:", delErr)
	}
	// return message
	return secret
}
func saveMessage(message []byte) string {
	// get new random key
	key, iv, err := getRandomKey()
	if err != nil {
		// TODO handle error
		return ""
	}
	// encrypt message with key
	msgb := []byte(message)
	block, err := aes.NewCipher(key)
	if err != nil {
		// TODO handle error
		return ""
	}
	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(msgb, msgb)
	// generate hash of key
	shash := getKeyHash(key, iv)
	// save to memory
	if disableMemoryCache == "false" {
		sm.Lock()
		secrets[shash] = msgb
		sm.Unlock()
	}
	// save to kvs
	saveErr := kvs.Save(shash, string(msgb))
	if saveErr != nil {
		fmt.Println("kvs.Save Error:", saveErr)
	}
	// return key
	return hex.EncodeToString(key) + "/" + hex.EncodeToString(iv)
}

func checkShowHelp(argv []string) bool {
	for _, arg := range argv {
		switch strings.ToLower(arg) {
		case "-h", "-?", "--help":
			return true
		}
	}
	return false
}

func showHelp(arg0 string) {
	fmt.Println("Usage:", arg0,
		`OPTIONS...
	OPTIONS Can be passed via environment variable or command line parameter
	
	List of Options and their defaults:
		port=8081
			tcp port for the http api to listen on
		srvDir=./public
			directory with public html/css to serve
		disableMemoryCache=false
			if this option is set to any value OTHER than "false" - the system will only save/load the secrets from the kvstore - it will not cache them in memory
			this will be slightly slower but should be set if you are running this as cluster all pointing at the same backend

		kvstore_type=filesystem
			type of kvstore to use
			vailid values are: 
				kvstore_type=filesystem (default)
				kvstore_type=sqlite
				kvstore_type=postgres
				kvstore_type=etcd
				kvstore_type=dynamodb
				kvstore_type=s3
		kvstore_path=./secrets
			when kvstore_type=filesystem - the file path to store messages

		kvstore_postgres_connectionstring=
			when kvstore_type=postgres - the connection string to use to connect to the database
			example: postgres://username:password@localhost:5432/database_name
			when using postgres this option MUST be specified
		kvstore_postgres_table=secrets
			when kvstore_type=postgres - the name of the table to use
			it will create the table if it doesn't exist

		kvstore_etcd_endpoints=127.0.0.1:2379
			when kvstore_type=etcd - a comma seperated list of etcd cluster endpoints to connect to
		kvstore_etcd_keyprefix=/secrets/
			when kvstore_type=etcd - a key prefix to add ot the name of the keys saved in the etcd cluster

		kvstore_dynamodb_table=
			when kvstore_type=dynamodb - the name of the dynamodb table to use
			when using dynamodb this option MUST be specified
			the table MUST have a primary key called "key"
			dynamodb uses the default AWS credential loading
			you can use environment variables, instance roles, etc
			see the AWS documentation for more details

		kvstore_s3_bucket=
			when kvstore_type=s3 - the name of the s3 bucket to use
			when using s3 this option MUST be specified
		kvstore_s3_prefix=secrets/
			when kvstore_type=s3 - a prefix to add to the key names in the bucket

		`)
}

func getSetting(args *map[string]string, key, sdefault string) (res string) {
	res = envopt.GetSetting(args, key, sdefault)
	fmt.Println("envopt:", key, "==", res)
	return
}

func initKvs(args *map[string]string) (kvs kvstore.KVStore, err interface{}) {
	kvs = nil
	err = nil
	kvstore_type = getSetting(args, "kvstore_type", "filesystem")
	switch kvstore_type {
	case "filesystem":
		kvstore_path = getSetting(args, "kvstore_path", "./secrets")
		kvs, err = kvstore.InitFilesystem(kvstore_path, true, 0700, 0600)
		return
	case "postgres":
		kvstore_postgres_connectionstring := getSetting(args, "kvstore_postgres_connectionstring", "")
		if kvstore_postgres_connectionstring == "" {
			err = "ERROR: kvstore_postgres_connectionstring is required when kvstore_type=postgres"
			return
		}
		kvstore_postgres_table := getSetting(args, "kvstore_postgres_table", "secrets")
		kvs, err = kvstore.InitPostgres(kvstore_postgres_connectionstring, kvstore_postgres_table, true)
		return
	case "etcd":
		kvstore_etcd_endpoints := getSetting(args, "kvstore_etcd_endpoints", "127.0.0.1:2379")
		kvstore_etcd_endpoints_array := strings.Split(kvstore_etcd_endpoints, ",")
		kvstore_etcd_keyprefix := getSetting(args, "kvstore_etcd_keyprefix", "/secrets/")
		kvs, err = kvstore.InitEtcd(kvstore_etcd_endpoints_array, kvstore_etcd_keyprefix)
		return
	case "dynamodb":
		kvstore_dynamodb_table := getSetting(args, "kvstore_dynamodb_table", "")
		if kvstore_dynamodb_table == "" {
			err = "ERROR: kvstore_dynamodb_table is required when kvstore_type=dynamodb"
			return
		}
		kvs, err = kvstore.InitDynamoDB(kvstore_dynamodb_table)
		return
	case "s3":
		kvstore_s3_bucket := getSetting(args, "kvstore_s3_bucket", "")
		if kvstore_s3_bucket == "" {
			err = "ERROR: kvstore_s3_bucket is required when kvstore_type=s3"
			return
		}
		kvstore_s3_prefix := getSetting(args, "kvstore_s3_prefix", "secrets/")
		kvs, err = kvstore.InitS3(kvstore_s3_bucket, kvstore_s3_prefix)
		return
	default:
		err = "ERROR: invalid kvstore_type"
		return
	}
}

// rest api
func main() {
	// load options
	argv := os.Args[1:]
	if checkShowHelp(argv) {
		showHelp(os.Args[0])
		return
	}
	ops := envopt.ParseStrings(argv)
	port = ":" + getSetting(ops, "port", "8081")
	srvDir = getSetting(ops, "srvDir", "./public")
	disableMemoryCache = getSetting(ops, "disableMemoryCache", "false")
	// deduce files from srvDir
	submitHtmlFile = srvDir + "/submit.html"
	cssFileDark = srvDir + "/pico.classless.min.css"
	viewHtmlFile = srvDir + "/view.html"
	// load overrides from hidden options (why would anyone need this?)
	submitHtmlFile = getSetting(ops, "submitHtmlFile", submitHtmlFile)
	cssFileDark = getSetting(ops, "cssFileDark", cssFileDark)
	viewHtmlFile = getSetting(ops, "viewHtmlFile", viewHtmlFile)
	// init kvs system
	var kvs_err interface{}
	kvs, kvs_err = initKvs(ops)
	if kvs_err != nil {
		fmt.Println("ERROR: couldn't init kvstore:", kvs_err)
		return
	}
	defer kvs.Close()
	// map to hold in-memory cache of secrets
	secrets = make(map[string][]byte, 100)
	// init gin http server
	r := gin.Default()
	r.SetTrustedProxies([]string{"127.0.0.1"})
	r.StaticFile("/", submitHtmlFile)
	r.StaticFile("/pico.classless.min.css", cssFileDark)
	r.POST(smsgpath, func(c *gin.Context) {
		data, _ := c.GetRawData()
		key := saveMessage(data)
		c.JSON(200, gin.H{
			"msgid": key,
		})
		//c.String(200,"POST /msg/\n%s\n",key)
	})
	r.GET(smsgpathid, func(c *gin.Context) {
		c.File(viewHtmlFile)
	})
	r.DELETE(smsgpathid, func(c *gin.Context) {
		key := c.Param("key")
		iv := c.Param("iv")
		msg := loadMessage(key, iv)
		c.Data(200, "application/json", msg)
		//c.String(200,"DELETE /msg/%s\n%s\n", msgid,msg)
	})
	r.Run(port)
}
