package kvstore

import (
	"context"
	"github.com/go-pg/pg/v10"
	"strings"
)

type kvstore_postgres struct {
	tableName string
	db        *pg.DB
}

func safeTableNamePostgres(tn string) string {
	return `"` + strings.Replace(tn, `"`, `""`, -1) + `"`
}

// connStr example: postgres://username:password@localhost:5432/database_name
func InitPostgres(connStr, tblName string, createTable bool) (pkvs *kvstore_postgres, err interface{}) {
	err = nil
	pkvs = nil
	var kvs kvstore_postgres
	kvs.tableName = safeTableNamePostgres(tblName)

	opt, opterr := pg.ParseURL(connStr)
	if opterr != nil {
		err = opterr
		return
	}

	db := pg.Connect(opt)

	conerr := db.Ping(context.Background())
	if conerr != nil {
		err = conerr
		return
	}

	if createTable {
		createTableSQL := "CREATE TABLE IF NOT EXISTS " + kvs.tableName + " (key text unique, value bytea) ;"
		_, cterr := db.Exec(createTableSQL)
		if cterr != nil {
			err = cterr
			return
		}
	}

	kvs.db = db

	pkvs = &kvs
	return
}

func (kvs *kvstore_postgres) Save(key, value string) (err interface{}) {
	err = nil
	sqlQuery := "INSERT INTO " + kvs.tableName + " VALUES ( ? , ? ) ON CONFLICT (key) DO UPDATE SET value = ?"
	_, err = kvs.db.Exec(sqlQuery, key, []byte(value), []byte(value))
	return
}

func (kvs *kvstore_postgres) Load(key, default_value string) (value string, err interface{}) {
	err = nil
	var qres struct {
		Value []byte
	}
	_, queryerr := kvs.db.QueryOne(&qres, "SELECT value FROM "+kvs.tableName+" WHERE key = ?", key)
	if queryerr != nil {
		return default_value, queryerr
	}
	return string(qres.Value), nil
}

func (kvs *kvstore_postgres) Delete(key string) (err interface{}) {
	err = nil
	sqlQuery := "DELETE FROM " + kvs.tableName + " WHERE key = ?"
	_, err = kvs.db.Exec(sqlQuery, key)
	return
}

func (kvs *kvstore_postgres) Close() (err interface{}) {
	err = nil
	if kvs.db == nil {
		return nil
	}
	kvs.db.Close()
	kvs.db = nil
	return nil
}
