package kvstore

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type kvstore_dynamodb struct {
	svc       *dynamodb.DynamoDB
	tableName string
}

// uses default aws credential loading i.e. AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN AWS_REGION
// the table must already exist with primary key "key"
func InitDynamoDB(tableName string) (pkvs *kvstore_dynamodb, err interface{}) {
	err = nil
	pkvs = nil
	var kvs kvstore_dynamodb

	kvs.svc = dynamodb.New(session.New())
	kvs.tableName = tableName

	pkvs = &kvs
	return
}

func (kvs *kvstore_dynamodb) Save(key, value string) (err interface{}) {
	var kvconv struct {
		Value []byte
	}
	kvconv.Value = []byte(value)
	kvm, kvmerr := dynamodbattribute.Marshal(kvconv)
	if kvmerr != nil {
		err = kvmerr
		return
	}
	input := &dynamodb.PutItemInput{
		Item: map[string]*dynamodb.AttributeValue{
			"key": {
				S: aws.String(key),
			},
			"value": kvm.M["Value"],
		},
		ReturnConsumedCapacity: aws.String("TOTAL"),
		TableName:              aws.String(kvs.tableName),
	}
	_, err = kvs.svc.PutItem(input)
	return
}

func (kvs *kvstore_dynamodb) Load(key, default_value string) (value string, err interface{}) {
	value = default_value
	err = nil
	var Result struct {
		Value []byte
	}
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"key": {
				S: aws.String(key),
			},
		},
		TableName: aws.String(kvs.tableName),
	}
	result, gierr := kvs.svc.GetItem(input)
	if gierr != nil {
		return default_value, gierr
	}
	if result.Item == nil {
		return default_value, "GetItem.Item was nil"
	}
	umerr := dynamodbattribute.UnmarshalMap(result.Item, &Result)
	if umerr != nil {
		return default_value, umerr
	}
	return string(Result.Value), nil
}

func (kvs *kvstore_dynamodb) Delete(key string) (err interface{}) {
	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"key": {
				S: aws.String(key),
			},
		},
		TableName: aws.String(kvs.tableName),
	}
	_, err = kvs.svc.DeleteItem(input)
	return
}

func (kvs *kvstore_dynamodb) Close() (err interface{}) {
	return nil
}
