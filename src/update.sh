#!/bin/bash
cd "${BASH_SOURCE%/*}" &&
cp -f go.mod.latest go.mod &&
rm -rf vendor &&
go mod tidy -v &&
go mod vendor -v &&
echo ready to run ../make.sh
