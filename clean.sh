#!/bin/bash
(
  cd "${BASH_SOURCE%/*}"
  realpath bin || exit 1
  read -erp 'do you want to delete the above directory? [y/N] ' DELETE_ALL
  [[ "$DELETE_ALL" == y* ]] && rm -rf bin
)
