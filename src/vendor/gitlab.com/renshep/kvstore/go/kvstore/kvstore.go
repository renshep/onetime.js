package kvstore

type KVStore interface {
	Save(key, value string) (err interface{})
	Load(key, default_value string) (value string, err interface{})
	Delete(key string) (err interface{})
	Close() (err interface{})
}
