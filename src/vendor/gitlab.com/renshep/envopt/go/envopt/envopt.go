package envopt

import (
	"os"
	"strings"
)

type storage_map = map[string]string

func ParseStrings(input []string) *storage_map {
	storage := make(storage_map)
	for _, item := range input {
		split := strings.SplitN(item, "=", 2)
		if len(split) > 1 {
			storage[split[0]] = split[1]
		}
	}
	return &storage
}

func GetSetting(storage *storage_map, key string, default_value string) (data string) {
	if storage != nil {
		d, ok := (*storage)[key]
		if ok {
			return d
		}
	}
	d, ok := os.LookupEnv(key)
	if ok {
		return d
	}
	return default_value
}
