#!/bin/bash
(
cd "${BASH_SOURCE%/*}"/src
go fmt
go build "$@" secret.go
mkdir -p ../bin
mv secret ../bin/
cd ..
rm -f bin/public
ln -s ../html-src bin/public
echo "[Unit]
Description=onetime service
After=network.target
[Service]
Type=simple
WorkingDirectory=$(realpath bin)
ExecStart=$(realpath bin/secret)
Restart=always
[Install]
WantedBy=multi-user.target" > ./onetime.service.example
echo 'to run:
cd '$(realpath bin)'
./secret'
)

