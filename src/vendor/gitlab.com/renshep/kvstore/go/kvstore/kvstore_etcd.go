package kvstore

import (
	"context"
	"go.etcd.io/etcd/client/v3"
	"time"
)

type kvstore_etcd struct {
	cli       *clientv3.Client
	ctx       context.Context
	keyprefix string
}

func InitEtcd(endpoints []string, keyprefix string) (pkvs *kvstore_etcd, err interface{}) {
	err = nil
	pkvs = nil
	var kvs kvstore_etcd
	kvs.keyprefix = keyprefix

	cli, clierr := clientv3.New(clientv3.Config{
		Endpoints:   endpoints,
		DialTimeout: 2 * time.Second,
	})
	if clierr != nil {
		err = clierr
		return
	}
	kvs.cli = cli
	kvs.ctx = context.Background()

	pkvs = &kvs
	return
}

func (kvs *kvstore_etcd) Save(key, value string) (err interface{}) {
	_, err = kvs.cli.Put(kvs.ctx, kvs.keyprefix+key, value)
	return
}

func (kvs *kvstore_etcd) Load(key, default_value string) (value string, err interface{}) {
	err = nil
	res, reserr := kvs.cli.Get(kvs.ctx, kvs.keyprefix+key)
	if reserr != nil {
		return default_value, reserr
	}
	for _, ev := range res.Kvs {
		return string(ev.Value), nil
	}
	return default_value, nil
}

func (kvs *kvstore_etcd) Delete(key string) (err interface{}) {
	_, err = kvs.cli.Delete(kvs.ctx, kvs.keyprefix+key)
	return
}

func (kvs *kvstore_etcd) Close() (err interface{}) {
	err = nil
	if kvs.cli == nil {
		return nil
	}
	kvs.cli.Close()
	kvs.cli = nil
	return nil
}
